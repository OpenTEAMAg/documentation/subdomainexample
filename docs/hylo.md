# Hylo

Hylo is a free and open-source community platform for purpose-driven groups. Our web and mobile apps enable discussions, requests, offers, resources, projects, events, geographic maps, rich member directories, and direct messaging.<br>

<img src = https://gitlab.com/OpenTEAM1/fellows-tutorials/-/raw/master/docs/img/Screen_Shot_2022-07-15_at_4.40.11_PM.png>

## Additional Resources

<ul>
<li><a href="https://hylozoic.gitbook.io/hylo/guides/farm-profiles-on-hylo">Farm Profiles on Hylo</a></li>
<li><a href="https://hylozoic.gitbook.io/hylo/guides/hylo-user-guide">Hylo User Guide</a></li>
<li><a href="https://hylozoic.gitbook.io/hylo/guides/hylo-moderator-guide">Hylo Moderator Guide</a></li>
<li><a href= "https://docs.google.com/document/d/1oUsySFDRzHzDjE8hqPvXWww4-AhO3leba8EfZ43yIlc/edit?usp=sharing">SurveyStack < > Hylo Integration</a></li>
<li><a href= "https://youtu.be/BA_V7pVStYk">OpenTEAM In-Depth Presentation (May, 2022)</a></li>
<li><a href= "https://youtu.be/nf6M65uHDh4">OpenTEAM In-Depth Presentation (2020)</a></li>
<li><a href = "https://us02web.zoom.us/rec/share/-AG6LVl5kkEkeym5JBTQdsMp04M4kRV9t_iSpFF87iV5LDqZv6GD_I1WdZLiWfD6.DfgeJWc7qP1l0pwT?startTime=1658426780000">Fellows Orientation Recording</a></li>
</ul>

## Developer Walk-Through (with Clare Politano)

 <p style="color:#FF0000";><strong>From OpenTEAM Fellows Orientation -- July, 2022</strong></p><br>

#### Required Equipment
- Computer

### Rough Presentation Agenda - [Slide Deck](https://docs.google.com/presentation/d/1TDPj4ER00HYB3v4F7ncIzGXmhbo1urwLTOB7HhlvPxs/edit?usp=sharing)

#### Intro to Hylo [10 mins]

- What it is, our purpose, vision, values<br>
- What it’s for, features, use cases, examples<br>
- **Hylo and OpenTEAM**<br>

    - Why we need a community tool for producers<br>
    - The connection with other tools + Ag data wallet + digital coffee shop<br>

#### Hylo Demo [15 minutes]

- General Hylo features<br>
- Farm-specific features<br>
- **Q & A**

#### How Fellows can use Hylo [10 mins]

- In their Pilot communities<br>

    - Post events: Fellows can create network Events on Hylo and create Projects for efforts in their network<br>

        - Create regular recurring online or in-person events to support your network of producers in meeting goals, can be learning events etc<br>

    - Post discussions for hot topics in their network<br>
    - Fellows can Ask producers to complete a few suggested tasks on Hylo:<br>

        - Complete your profile with Skills & What I’m Learning<br>
        - Create a geographic outline for your farm<br>
        - Make a post introducing themselves<br>
        - Make a post with a Request on something they need help with (such as asking a question for advice)<br>
        - Make a post with an Offer for something they are willing to help people with<br>
        - Encourage producers to discover other producers in their network and reach out according to peoples’ Opportunities to Collaborate<br>

- Amongst themselves<br>

    - Tech Feedback - share how tools are working<br>

- **Discussion:** *What ideas do you have for using Hylo in your network?*<br>

#### How SurveyStack connects to Hylo [5 mins]

- Question from CFDN - how will data use agreements for Surveystack & Hylo be communicated to producers?

#### Hylo Tutorial [45 mins]

- Create individual account on Hylo<br>
- Create group for your producer network on Hylo (if it doesn’t already exist)<br>
- **SS Integration** (covered in earlier session) - [Guide Here](https://docs.google.com/document/d/1oUsySFDRzHzDjE8hqPvXWww4-AhO3leba8EfZ43yIlc/edit#)<br>

    - Step 1: Creating a SurveyStack group to represent your network<br>
    - Step 2: Integrate with New Group on Hylo (for ease here, we are having the fellows create a new group on Hylo w/in surveystack, rather than integrating a group that was already created in Hylo).<br>
    - Step 3: Create your Survey in SurveyStack<br>

        - Add question sets: "Hylo Onboarding" (which now includes the welcome question), and "Common Profile"<br>

    - Step 4: Go through the Fellows Training Common Onboarding survey and create a farm profile<br>


- **Using Hylo** - [Farm Profiles Guide Here](https://docs.google.com/document/d/14Rl184ttZEiUY5zOPKlfKJgnGcn7CC78RZyuoVEldaA/edit#heading=h.tq7hhfjgsndd)<br>

    - Adding more to your individual profile<br>
    - Posting discussion, requests, etc<br>
    - Starting a project<br>
    - Posting an event<br>
    - Using the map<br>
    - Admin settings, inviting people<br>
    - DMs<br>
    - Adding more to your farm About page<br>
    - Search for farms near you / with similar products / techniques<br>
    - Message a farmer to collaborate<br>

#### Recommended Strategies for Producer Onboarding [5 mins]

- Get them onboarded<br>
- Rian suggests waiting until there’s a critical mass of farmers onboarded to Hylo<br>
- Make a launch plan<br>

    - Develop your hub pages as thoroughly as possible before launch<br>
    - Launch in Q4<br>

- Invite producers to log in and participate in something exciting and valuable!<br><br>










