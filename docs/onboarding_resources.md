# OpenTEAM Important Links

### Broad OpenTEAM Resources
- [OpenTEAM Website](https://openteam.community/)
- [OpenTEAM In-Depth Series](https://openteam.community/in-depth/)
- [OpenTEAM Key Terms and Definitions](https://docs.google.com/document/d/1OzCPGnEPAjyNlRz1hbft65A6v6eX5G5kXNJ_74byDHo/edit)
- [OpenTEAM 2021 Progress Report](https://openteam.community/#members)
- [OpenTEAM Equity Collabathon Resource Guide](https://openteam-equity-resource.webflow.io/)<br>

### Fellows-Specific Resources

- [Hubs & Tech Members Information -- Accessible with OpenTEAM Email](https://docs.google.com/spreadsheets/d/1ZNFaheTuUYU68-y2hassjA1HGJwvA_7gVUCSvtq5Mlk/edit?usp=sharing)
- [Genna's Fellowship Materials](https://drive.google.com/drive/folders/1nmkePpqWaQkj6upFnf7I-J9PVQC2omLD?usp=sharing)
- [Jeanne's Fellowship Materials](https://drive.google.com/drive/folders/1KZyQF8LQ8dhgzUBIuYV04mUVAiZTK476?usp=sharing)
- [Jeff's Fellowship Materials](https://drive.google.com/drive/folders/1Fp3RXpdkq-2IoqFdiwfm2J4rznYDY6nf?usp=sharing)
- [Sara's Fellowship Materials](https://drive.google.com/drive/folders/1zOS3ZVCVrkVmWQbjiO4AEVyDad8RFmm-?usp=sharing)
- [Steph's Fellowship Materials](https://drive.google.com/drive/folders/1pKoVeUo1CkTBGkQX5dSM4TQ5uhbqEN1Q?usp=sharing)
<br><br>



