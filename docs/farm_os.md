# FarmOS

[FarmOS](https://farmos.org/) is an open source web based application for farm management, planning, and record-keeping developed by a community of farmers, developers, researchers, and organizations. <br><br>

<img src = https://gitlab.com/OpenTEAM1/fellows-tutorials/-/raw/master/Screen_Shot_2022-07-17_at_6.07.32_PM.png>

## Additional Resources

<ul>
<li><a href="https://farmos.org/blog/">FarmOS Community Blog</a></li>
<li><a href="https://farmos.org/blog/2022/getting-started">Getting Started with FarmOS</a></li>
<li><a href="https://farmos.org/guide/">FarmOS User Guide</a></li>
<li><a href = https://farmos.org/community/monthly-call>FarmOS Monthly Call</a></li>
<li><a href= "https://www.youtube.com/watch?v=KLQfKtsLDxA">GOAT webinar series (Original Video)</a></li>
<li><a href= "https://www.youtube.com/watch?v=MkAYvI5iUqc">GOAT webinar series (Updated Video, FarmOS 2.0)</a></li>
<li><a href="https://farmos.discourse.group/t/first-blog-posts-brainstorm-dev-call-2022-06-09/1264">FarmOS Discourse Page</a></li>
<li><a href = https://docs.google.com/presentation/d/19N7k1ZEJtVQtHx7soe8u0ScLgknnALooLRPuh__LNhQ/edit#slide=id.g1194b89e947_2_57>FarmOS 2022 Fellows Orientation Slide Deck</a></li>
<li><a href = https://drive.google.com/file/d/1YlrwhxcM1Hp-VF9nGOwCsBDkdkhtbDMD/view?usp=sharing>Fellows Orientation Recorded Session</a></li>
</ul>

## Developer Walk-Through (with Mike Stenta)

 <p style="color:#FF0000";><strong>From OpenTEAM Fellows Orientation -- July, 2022</strong></p><br>

#### Required Equipment
- Computer

### Agenda

- FarmOS Overview<br>

    - Background/founding<br>
    - General overview of tool and it’s place within OT ecosystem<br>
    - Transition to 2.0 (highlighting major changes and upgrades)<br>

- Login to Demo FarmOS Instance (from common onboarding session)<br>
- Walk through the FarmOS Tutorial - Written out below (everyone)<br>
    
    - Mike to share his screen at the same time as fellows navigate through the tutorial on their computers<br>

- Discuss how standard protocols will work for entering data in SurveyStack to be pushed to FarmOS (in development)<br>

    - When will this use case be ideal, and when might farmers be better served working directly in FarmOS (entering data via quick forms)?<br>

- Q&A

### Tutorial

#### Login

Enter the URL for the Fellows FarmOS account. Enter your username and password, and click “log in” to begin using farmos. 

#### Dashboard & Navigation

<img src = https://gitlab.com/OpenTEAM1/fellows-tutorials/-/raw/master/docs/img/Screen_Shot_2022-07-17_at_6.15.07_PM.png>

When you log in you will see the FarmOS dashboard. The dashboard includes:

**Map:**<br>

A map of your farm with all the fields. You can use the map to navigate and learn about the fields and view and manipulate assets and associated logs. <br>

**Left Sidebar:**<br>

- Quick Forms: Quick forms make it easy to enter and record common farm activities and events. This will only be visible if you have the quick modules enabled. To enable modules, you must be the “Account admin” then  you can  install via Settings > Modules.<br> 

    - *Note, Access to Settings > Modules is restricted by permission. Only the "Account Admin" role has access to it (on Farmier instances). This will be the farmer in most cases, if they are the ones creating the instance (or we create the instance on their behalf and make them the "registrant", which is typically how it's done).*<br>

- Locations: Locations are all the fields, pastures, greenhouses and buildings on the farm. Notice that these can be organized in drop down menus based on farmer preference.<br>
- Records: Under the Map you will see buttons to add area, add asset and add log, today’s date, upcoming tasks, late tasks and active plans, metrics, and logs.<br>

    - Assets: Assets are the “management units” -  crops, animals, equipment and sensors on the farm.<br>
    - Logs: Logs are the activities on the farm - activities, births, harvests, inputs, maintenance, medical, observations, seedings, soil tests, and transplantings.<br>

        - Quantities: record quantitative measurements within a log to track data over time for future analysis (enter a measurement, value, unit, or label)<br>

    - Add Asset or Log button: Click the down arrow to add a log or an asset. You can use this button to manually add a new plant, animal, or record.<br>

- People: People should include anyone who has access to your FarmOS account. Look through the different roles and add users appropriately. These can include farm owners, managers, employees, technical service providers and more. The role that the person is assigned will determine their permissions within the software.<br>
- Administration: this is where you can go to manage your taxonomies (e.g. plant types, animal types, measurement units, etc.)<br> 

#### Mapping Locations: Add a location

1. Click the “add Asset” button<br>
2. Add Land<br>
3. Before entering info, scroll through the page. Notice the different land type options and additional information you can enter for your asset.<br>
    - While there are a lot of options, the only required fields are Name and Land Type, so you can fill in more later if you want (including geometry, or shape of the parcel of land,- sometimes you just want to record a Log against a place before you've mapped it).<br>
4. Create a field name<br>
5. Draw a shape on the map using the geometry function.<br>
6. You will use this field for your plantings in the next step.<br>
7. Save and make sure you see your new field on the map in the home screen.<br>

#### Create a planting using the Quick Form function

1. Click on “Quick Forms”, then “Planting”.<br>
2. Enter the current year in the “Season” field (eg: “2022”) - You can also divide your seasons into “2022 Summer” / “2022 Winter” if that is how you think about them.<br>
3. Enter the crop and variety into the “Crop/Variety” field (eg: “Chioggia Beet”)<br>
4. Check the “Seeding” and “Transplanting” (when applicable) checkboxes (and optionally “Harvest” if you want to plan ahead your first harvest).<br>
5. Under the “Seeding” / “Transplanting” / “Harvest” vertical tabs on the left, fill in the information about each event<br>
    - Date<br>
    - Location (Start typing the name of the Land asset you created above and you will see it appear in a dropdown. Click on the item in the dropdown to populate the field.)<br>
    - Quantity (optional, but recommended)<br>

        - Measure (eg: “Count”)<br>
        - Value (eg: “10”)<br>
        - Units (eg: “72-plug trays”)<br>

    - Notes (optional)<br>
    - Completed (check this if you have already performed the task)<br>

6. Click “Submit” - you will then see links to the Plant asset and Log(s) that were created.<br>
7. Click on the name of the Plant asset that was created.<br>
    - Notice that it shows basic information.<br>

8. Click on the “Logs” tab at the top of the Asset page.<br>
    - Notice the Seeding/Transplant/Harvest logs. When a seeding/transplanting log is marked as “Done”, the plant will “move” to the location specified in the log, and therefore will be visible/accessible from the map.<br>
    - The checkboxes next to logs allow you to perform “bulk actions” including marking them as done.<br>

9. Go back to the dashboard and notice that “Pending” logs show under the map. They can be marked as done from here as well.<br>
10. Go to Assets > Plant and you will see your new Plant asset in the list. Click on its name to get back to its record.<br>
11. Notice that there is a dropdown to add logs of specific types (eg: Add Harvest)<br>
    - Click the “Edit” tab to see the full list of fields available on Plant assets.Name (modify the name)<br>
    - Crop/Variety (modify the crop/variety)<br>
    - Status Active means that this planting has not occurred yet; archived means that the planting event already occurred.<br>
    - Flags: Check Priority as these need to be planted this week<br>
    - Season (modify the season)<br>
    - Check out the other options - some are more relevant for land, animals and observations<br>
    - Notice at the bottom “Revision Notes.” This feature is beneficial to use to let your team know when and why an asset was edited or revised.<br> 

#### Create a log using the asset we just created. 

1. Navigate to the plant asset record.<br>
2. Click “add Harvest Log” button<br>
3. Enter a descriptive name for the log (up to you! eg: “Harvest 10 lbs beets”)<br>
4. Now we will create a planned/future harvest log using activity logs with the asset we just created. Choose a time for harvesting next week.<br>
5. Status: Pending (This means you have not completed the task yet)<br>
6. Flags: Priority<br>
7. Assign to one of your teammates!<br>
8. Quantity: Add new quantity: choose count, you want your teammate to harvest 10 lbs beets<br>
9. Assets: Make sure your new Beet asset is displayed, notice you can add additional assets<br>
10. Location: Type in your newly created field<br> 
11. Click Save. You’ve created a harvest log!<br>
12. Click back into the log: notice you can view, edit and see the revisions<br>
    - You can go back in to edit once the task has been completed, make sure to add in a note too.<br>  
    - Try this and then take a look at the revisions tab again, do you see a record of your edits?<br>

#### Another way to create a log : Use the Create Asset & Log function

1. Click Add Asset<br>
2. Add Plant<br>
3. Name: Enter “Beet Summer 2022”<br> 
4. Crop/Variety: Chioggia Beet<br>
5. Status: Active (Active means that this planting has not occurred yet; archived means that the planting event already occurred).<br>
6. Flags: Check Priority as these need to be planted this week<br>
7. Season: 2022<br>
8. Check out the other options - some are more relevant for land, animals and observations<br>
9. Notice at the bottom “Revision Notes.” This feature is beneficial to use to let your team know when and why an asset was edited or revised.<br> 
10. Save<br>
11. You have now created an asset.<br> 
12. A screen will appear with the name of the Asset, information about the Asset, and an “Add Activity Log” button<br>

#### Create a log using the asset we just created. 

1. Click “add Activity Log” button<br> 
2. The name will generate automatically as you fill out the rest of the form<br>
3. Timestamp: Choose a time next week<br>
4. Status: Pending (This means you have not completed the task yet)<br>
5. Flags: Priority<br>
6. Assign to one of your teammates!<br>
7. Quantity: Add new quantity: choose count, you want your teammate to plant 100 beets<br>
8. Log category: Plantings<br>
9. Assets: Make sure your new Beet asset is displayed, notice you can add additional assets<br>
10. Location: Type in your newly created field<br> 
11. Click Save. You’ve created a planting!<br>
12. Click back into the log: notice you can view, edit and see the revisions<br>
    - You can go back in to edit once the task has been completed, make sure to add in a note too.<br>  
    - Try this and then take a look at the revisions tab again, do you see a record of your edits?<br>

#### Searching for Records & CSV download

1. On the left sidebar of the screen, click on Records<br> 
    - Within this section you can view, add, delete and edit your assets, logs and quantities.<br>
2. Click Assets<br>
    - Search for your carrots<br>
        - Notice you can see and access your carrots on the map and can click into the asset in the table below the map. When would it be helpful to view an asset in map-view as opposed to table-view?<br> 
3. Search by Location<br>
    - Click “Locations” on the left (or click on the shape in the dashboard map), then click the name of the Location to go to its asset record.<br>
    - Click the “Assets” tab to view assets currently located there.<br>
    - Click the “Logs” tab to view logs that reference the location.<br>
4. Click on logs<br> 
    - Explore and download the CSV of your logs<br>


Animal Logs: You can also use farmOS to keep track of your livestock records and moves.<br> 
<br>

Using the guide above, create an animal asset, add them to a field and move them to a new field. 

#### How to think about data in FarmOS

“Data is intended to be iterative. Assets can be archived. Logs flow into the past. Your records can become better and more refined over time!” - M. Stenta


--- 
> #### Worksheet # 1
>
> <strong>In order to gain a more robust understanding of the use cases of these tools, we are going to compile a Use Case document as a reference. Discuss these questions about the applicability of this tool, using online resources and your experience using FarmOS:</strong> 
> </br>
> </br>
> <ul>
> <li>How can this tool help a farmer or rancher?</li>
> </br>
> </br>
> <li>For whom is this tool designed?
> </br>
> </br>
> <ul>
> <li>What kind of farmer is this tool good for? </li>
> </br>
> </br>
> <li>What kind of farm is this tool good for?</li>  
> </ul>
> </br>
> </br>
> <li>When would it be most useful?</li>
> </br>
> </br>
> <li>What are the tool’s privacy policies? </li><br><br>
> <li>How does this tool interact with other OpenTEAM tools (e.g. SurveyStack, Hylo, the Ag Data Wallet, Digital Coffeeshop, etc.)</li> 
> </br>
> </br>
> </ul>

--- 
