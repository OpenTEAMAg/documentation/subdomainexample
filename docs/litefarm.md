# LiteFarm

<img src = https://gitlab.com/OpenTEAM1/fellows-tutorials/-/raw/master/docs/img/Screen_Shot_2022-07-05_at_1.03.21_PM.png>

[LiteFarm](https://www.litefarm.org/) is a Canadian-based tool used to keep track of on-farm management decisions, including task assignment and labor division, biodiversity, profits, certifications, and more.<br><br>


The application was designed with certification processes in mind, but note that because LiteFarm is Canadian, it is not possible to accommodate American certifications at this time. 

## Additional Resources

<ul>
<li><a href="https://www.litefarm.org/resources-for-users">Resources for Users</a></li>
<li><a href= "https://www.litefarm.org/resources-for-contributors">Resources for Contributers</a></li>
<li><a href= "https://youtu.be/CQZDugFwQpQ">OpenTEAM In-Depth Presentation</a></li>
</ul>

## Onboarding Process

 <p style="color:#FF0000";><strong>Last Updated July, 2022</strong></p><br>

For this activity, you will be setting up Wolfe's Neck West Bay vegetable plots to track farm management decisions. You are a farm manager, and the farm is owner-operated. You will not be pursuing any certifications this season. 

#### Required Equipment
- Computer

#### Step 1: Account & Farm Setup 

**1.1.** Access [LiteFarm online](https://www.litefarm.org/) and create an account. <br>

**1.2.** Add a farm, using the Wolfe’s Neck Center address: 184 Burnett Rd, Freeport, ME 04032, United States.<br>

- *For this tutorial, you are a farm manager, and the farm is owner-operated. You will not be pursuing any certifications this season (LiteFarm is currently only set up for monitoring Canadian certification schemes).*<br> 

**1.3.** Add your teammates so you can all work on the same farm.<br>

- *Everyone in your group will need to make accounts, but only 1 person needs to create a farm. Once someone creates the farm, they can add the teammates to the farm so you can all work on the same one.*

#### Step 2: Map Your Farm

Toggle to your farm map to see an overview of Wolfe’s Neck. Add in the vegetable plots in West Bay by drawing their outlines (you can use the map imaged below for reference – the veg plot is the small, 6-sided plot at the bottom of West Bay). This is an organic field, and you can call it “Veg Plot.” <br>

<img src = https://gitlab.com/OpenTEAM1/fellows-tutorials/-/raw/master/docs/img/PMTutorial_WNCFieldOutlines.png>

#### Step 3: Add Your Crops

Next we will add some crops. Wolfe’s Neck grows a variety of fruits and vegetables in these plots, but for this exercise, please plant strawberries.<br>

Now you can create a management plan for this crop:<br>

**3.1.** Add a crop plan<br>
**3.2.** Fill in the information to the best of your knowledge<br>

- *If the crop has not been planted already, “planting” will be added as a task*

**3.3.** Assign a task to your teammate. Head to “Tasks,” and assign one of your teammates to harvest 10 lbs of the strawberries you planted last week.<br> 

- *Once you plant the strawberries, “harvest” will be added as a task*<br>
- *Tasks can be managed in the task list, upper right-hand corner*<br>
- *You can peruse your active crops in the crop catalog (you will only see strawberries)*<br> 

#### Step 4: Upload Documents

If a farmer were to use LiteFarm for storing all of their farm’s information, it may be ideal to upload some documents so everything can be found in the same place. Go ahead and upload [some soil testing data](https://docs.google.com/spreadsheets/d/1KwpVioDkpYFD3bqoap6KHzlzJ9gRYhGeej0Mv20UbDY/edit#gid=0) to the site, under “Documents.”<br> 

#### Step 5: Explore the Finance Functionalities LiteFarm 

**5.1.** You’ve just bought a used No-Till Drill for $4,000. Add this as a machinery expense.<br> 
**5.2.** You just sold some of the strawberries you planted to a local restaurant for a total of $1,000. Add this as a new sale.<br>

#### Step 6: Check out LiteFarm Insights

Explore the other information in “Insights.” If you click on the small “i” on each page, LiteFarm will explain the information and from where it was collected.<br> 

-  In the Biodiversity tab, note that you can connect your LiteFarm account with an iNaturalist account (an app which allows you to identify, record, share, and discuss biodiversity findings on your land).  


--- 
> #### Worksheet # 1
> <strong>Discuss these questions about the applicability of this tool, using online resources and your experience using LiteFarm:</strong> 
> </br>
> </br>
> <ul>
> <li>How can this tool help a farmer or rancher?</li>
> </br>
> </br>
> </br>
> <li>For whom is this tool designed? 
> </br>
> </br>
> </br>
> <ul>
> <li>What kind of farmer is this tool good for?</li>
> </br>
> </br>
> <li>What kind of farm is this tool good for?</li>
> </ul>
> </br>
> </br>
> <li>When would it be most useful?</li>
> </br>
> </br>
> </br>
> <li>When may a farmer use LiteFarm instead of FarmOS?  
> </br>
> </br>
> </br>
> <ul>
> <li>What are the notable differences in user interface / experience? Is one easier to use than the other?</li>
> </br>
> </br>
> <li>Are there any notable differences in functionality (i.e. functions that one app has and the other doesn’t)?</li>
> </ul>
> </br>
> </br>
> <li>What are the tool’s privacy policies?</li>
> </br>
> </br>
> </br>
> </ul>

--- 