# PastureMap

<img src = https://gitlab.com/OpenTEAM1/fellows-tutorials/-/raw/master/docs/img/Screen_Shot_2022-07-13_at_2.44.12_PM.png><br>

 [PastureMap](https://pasturemap.com/) is a tool that can be used to track and plan management decisions on a ranch. This tool can be used on the web, iOS, and Android. For setting up your farm or ranch, access PastureMap online at https://pasturemap.com/ and create your account. 

## Additional Resources

<ul>
<li><a href="https://blog.pasturemap.com/getting-started-with-pasturemap">Getting Started with PastureMap</a></li>
<li><a href= "https://blog.pasturemap.com/?_gl=1*jrgv5w*_ga*NjAxNjE0NzIxLjE2NTc3Mzc4MzQ.*_ga_E829Z38LRK*MTY1NzczNzk0OC4xLjEuMTY1NzczODA5Ni4w&_ga=2.233116382.133809092.1657737834-601614721.1657737834">PastureMap Blog</a></li>
<li><a href = https://www.youtube.com/watch?v=92fj-qs9ixc>OpenTEAM In-Depth Presentation</a></li>
</ul>

## Onboarding Process

 <p style="color:#FF0000";><strong>Last Updated July, 2022</strong></p><br>

In this activity, you will be setting up an account using data from Wolfe’s Neck Center to model management decisions of the dairy herds and the land. 

#### Required Equipment
- Computer

#### Step 1: Integrate Map

Integrate the Wolfe’s Neck Center map by uploading a [kml file of the farm](https://drive.google.com/file/d/1zpEltJIt9kl4aAQN5PLToR6kx7tbz2iP/view?usp=sharing) (“Start from a KML file”). For future reference, if you don’t have a KML outline of your hub farm, you can create one in GoogleEarth and export it, or you can draw the farm/ranch outline manually.

#### Step 2: Draw Pastures

Use the “add pastures” drawing tool to draw in the Shire, the field behind the barn where we’re conducting orientation. We don’t keep cows in this field, but it is good to get practice with drawing field outlines. Use [this picture](https://drive.google.com/file/d/1TgBVuCc7mHMz-GgtsK2B7pj4M2-9yGZs/view?usp=sharing) for reference.

#### Step 3: Add Herds 

Next, you will upload two herds, for the milking herd and the cows of breeding age (not yet milking). Herds can be added manually or uploaded from a spreadsheet. 

**3.1.** For the milking herd, you will add them individually by clicking “new herd.”<br> 

- *Name the herd “Milking Herd”*<br> 
- *Add the total animals in herd: 48*<br>
- *Add the average animal weight: 950 lbs*<br>
- *Add the average daily gain: 1 lb/day*<br>
- *Add the estimated dry matter intake: 29 lbs/day (~3%)*<br>
- *Today is their first day on pasture.*<br> 

**3.2.** To add a herd later, toggle to the “herds” tab at the top of the page, and “add herd.”<br> 
**3.3.** Sometimes, farmers or ranchers may already store data about their herds in a spreadsheet. In these cases, you can upload this information directly into PastureMap. For adding the Breeding Age herd, you will upload from a spreadsheet.<br> 

- *Select “Start from XLS file” and download the template*<br> 
- *Fill out the template based on information from [this spreadsheet](https://docs.google.com/spreadsheets/d/1Zn_cUuCYx-cpQ2kDpwUIg7I1Etp2ERN3JorNwWYFFaY/edit#gid=0). Note that each field has a brief descriptor if you hover your mouse over the red triangle in the corner.*<br> 
- *Upload the completed template to PastureMap*<br>
- *Name the herd “Breeding Age” and add the target average daily gain and estimated dry matter intake*

    - *Target ADG: 1 lb/day*<br>
    - *Estimated DMI: 12 lbs/day*<br>

#### Step 4: Place Herds

You are done adding herds, and now it is time to place them. Place the milking herd in Brocklebank and the Breeding Age herd in the Bays. Drag and drop the herds into their correct pastures. <br>

- *Herds tend to graze each pasture for 1/2 day (16 hours)*<br>
- *Note that planned grazing days will likely change based on the size and composition of each pasture.*<br>

**4.1.** Notice, we have uploaded a KML of the large fields, not taking into consideration the subdivisions within the fields. Another important drawing feature of PastureMaps is the ability to add field subdivisions, which are very popular when cows are grazed on a rotational basis. Navigate to the “Pastures” tab, select “The Bays,” and “Subdivide.” You can use the map imaged below as a rough guide.<br> 

<img src = https://gitlab.com/OpenTEAM1/fellows-tutorials/-/raw/master/docs/img/PMTutorial_WNCFieldOutlines.png>

**4.2.** Repeat the process for Brocklebank as well.<br> 
**4.3.** Place the Breeding Age herd into the Western-most subdivision of the Bays pasture.<br>
**4.4.** Place the milking cows in one of the central subdivisions of the Brocklebank pasture.<br> 

#### Additional Notes

- You can adjust or retire subdivisions at any time.<br> 
- You can also choose to hay a pasture, and move herds between the subdivisions (real-time or planned for the future).<br>
- Now you have set up your ranch! Share access with your other teammates in settings. When it is time to create a map for another farm or ranch, note that you can add a new farm to your account in the top right-hand corner.<br>
- You can make adjustments to a herd at any time, marking movements of specific cows between herds, culls/slaughters, or updates to their weight in the “herd” tab.<br>
- Some ranchers have performed lab tests on their soils and these data can be relevant to grazing conditions. From “Add Items to the Map,” you can upload soil data records using PastureMaps’ template. (Note: don’t worry if your data doesn’t fit into the downloadable template perfectly. Just fill it in as best as you can).<br>

#### Step 5: Add Ranch Infrastructure

Next, we will add some infrastructure to the ranch map. Head out to West Bay and add infrastructure where you see it (water troughs, gates, bales, rain gauge, or any other relevant items).<br> 

Now you can explore your ranch with all of the data layers you have added. In the “data layers” tab, you can toggle between different information:<br> 

- Rest days: Rest days are the number of days in between grazing periods. This is to ensure each pasture has sufficient time to recover after a grazing event. 
- Animal days / acre: Animal days per acre is calculated by multiplying the number of animals by the number of days grazed, divided by the acreage. 
- Grazing days: the number of days the pasture has been grazed.
- Forage: The fields are coded by color based on available forage. For Wolfe’s Neck Center, anything below 1000 lbs / acre is considered low, and anything above 3000 lbs / acre is considered high. 1000-3000 lbs / acre is considered mid-range. 

### Step 6: Explore the Grazing Chart Functions

**6.1.** Add a key date to note when the calves will start being weaned from their mothers, so the herd must be split in two. This process would affect the whole ranch, and last about 2 weeks.<br> 
**6.2.** Next, add a planned move for the future, in which the chickens are relocated behind the Education Barn (“Hill Pasture”). This should happen on the morning August 10th, and they will graze there for a week.<br> 

--- 
> #### Worksheet # 1
>
> <strong>Discuss these questions about the applicability of this tool, using online resources and your experience using PastureMap:</strong> 
> </br>
> </br>
> <ul>
> <li>How can this tool help a farmer or rancher? </li>
> </br>
> </br>
> <li>For whom is this tool designed?
> </br>
> </br>
> <ul>
> <li>What kind of farmer is this tool good for?</li>
> </br>
> </br>
> <li>What kind of farm is this tool good for?</li>  
> </ul>
> </br>
> </br>
> <li>When would it be most useful?
> <ul><br><br>
> <li>When would you use the app on a computer vs. on a mobile device?</li><br><br><br> 
> </ul>
> <li>What are the tool’s privacy policies?</li>
> </br>
> </br>
> </br>
> </ul>

--- 




