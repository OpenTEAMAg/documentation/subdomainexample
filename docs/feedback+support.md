# Feedback & Tech Support Pipeline 

## Flow Chart of Support Options

<figure>
<img src = https://gitlab.com/OpenTEAM1/fellows-tutorials/-/raw/master/docs/img/OpenTEAM_Tech_Feedback_Map_-_OpenTEAM_Tech_Support__amp__Feedback_Pipeline_.jpg>
<figcaption><strong><i>Above is a flow chart of our feedback and support processes.</i></strong></figcaption>
</figure>

[Here is the link to this flow chart, which is easier to view in Miro.](https://miro.com/app/board/uXjVOqcUy1c=/?moveToWidget=3458764528326358247&cot=14)

## Written Process

**Step 1:** Review Existing Documentation<br>

- Visit the OpenTEAM Fellows Tutorial for the relevant tool<br> 
      - <i>These pages are meant to be an easy way to access onboarding exercises, details for common use cases, and developer-maintained resources for each tool.</i><br>

- Review existing posts in Hylo for similar user-documented issues<br>
      - <i>In particular, the [OpenTEAM Hub & Network](https://www.hylo.com/groups/openteam-hubs-networks/join/UWrYxYWCMu) and the [OpenTEAM Tech Feedback & Support](https://www.hylo.com/groups/openteam-tech-toolkit-group/join/p3Z0mm4fgL) groups may document posts with common user issues.</i>

**Step 2:** Ask for support for **road-blocking issues** (you cannot use the tool without help)<br>

- Post to the [OpenTEAM Tech Feedback & Support](https://www.hylo.com/groups/openteam-tech-toolkit-group/join/p3Z0mm4fgL) group in Hylo<br>

      - Note, this Hylo group is the fastest way for fellows and farm hubs to connect directly to tech tool developers. The OpenTEAM Tech Support Lead will also monitor this group to make sure that contributors get a response within 1-2 business days.<br>

- Email the OpenTEAM Service Desk (tech@openteam.community)<br>

      - The OpenTEAM Service Desk is a private option to ask for help from an OpenTEAM moderator. <br>

          - While we encourage you to post to Hylo, we recognize that some users are more comfortable with email-based support. For those users, please email our OpenTEAM Service Desk at tech@openteam.community. You will receive an automated response within a few minutes, followed by a technician response within 1-2 business days. <br>
          - For broad and/or recurring service desk issues, the OpenTEAM Service Desk moderator will post an update to the Hylo Tech Toolkit Group so that other users are aware of the issue and developers are notified. <br>

**Step 3:** Join OpenTEAM's Weekly Community Support Hour<br>

- Every Thursday, 2- 3 pm Eastern Time at <strong><a href = https://us02web.zoom.us/j/88612780187?pwd=MVVvQW5wQVNkQUczblkrMWVQcndoQT09>this zoom link</a></strong> <br><br>

**Step 4:** Complete a Feedback Submission Form in SurveyStack for **long-term issues**<br>

- ***A key note on process:** the Service Desk is meant to be an immediate (within 48 hour) source of support for confusion or points of failure. A survey response is a more long-term tool for broader points of feedback.*<br>
- After collecting a round of feedback submissions, an OpenTEAM moderator will again post a summary of key issues to Hylo so that both users and tech developers are aware of the issue and can track progress. This also provides a space for users to elaborate on their experience with an issue in the comments. <br>

## Community Support in Hylo

As fellows, you will have access to three core Hylo groups:

1. The [OpenTEAM Tech Feedback & Support](https://www.hylo.com/groups/openteam-tech-toolkit-group/join/p3Z0mm4fgL) is a space dedicated to issues, questions, and share-outs related to the OpenTEAM Tech Tools. 
2. The [OpenTEAM Hub & Network](https://www.hylo.com/groups/openteam-hubs-networks/join/UWrYxYWCMu) Hylo group is for broad, hub-related issues, discussions, or questions. Each fellow will also be added to a hub-specific Hylo group to enable communication and collaboration with your team on the ground.
3. Your Hub-specific Hylo group (which you will create yourself!) is for communication within your specific hub network.