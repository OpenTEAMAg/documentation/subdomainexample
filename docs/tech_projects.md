# Fellow Onboarding - Tech Projects

A significant portion of your role will involve learned how to use new tech tools, and then quickly turning around to teach others how to use those tools. To help get you comfortable with that process, you will be put into groups and assigned two tech tool tutorials to complete. Then, you will be expected to present each tool you were assigned to the rest of the fellows cohort.<br><br>

When planning your presentation, please consider the following guiding questions to help everyone understand the tool. You may project your screen if you’d like.

- What is this tool?
- Who is the tool designed for?
- How would you onboard farmers to this tool? Where do you anticipate struggle with onboarding? How can you mitigate this struggle (in person and remotely)?
- How would you “sell” this tool to a farmer?
- What is your favorite feature?
<br><br>