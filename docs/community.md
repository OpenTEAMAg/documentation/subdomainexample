# Approaches to Community Engagement

### Schedule DAY 2:

**9:00-9:15: Overview - Framing Today’s Session**<br>
**9:15-10:15: Tool Overview and Group Discussion**<br>

- After a brief intro, you will have 25 minutes to review three relevant tools we’ve found useful for our work at WNC as a hub. Then we will reconvene for open discussion.<br>

**10:15: Break**<br>
**10:30-12:00: Walking Tour of Wolfe’s Neck Center with Dave Herring, Executive Director**<br>

- Walking Tour - Starts and ends at Offices<br>
- <i><u>Fellows</u> → Keep morning discussion in mind, jot down notes and questions as you go for a debrief session after lunch</i><br>

**12:00-1:00: LUNCH!**<br> 
**1:00-1:45: WNC as a Case Study: Community Mapping Exercise**<br>
**2:00-2:30: Reflections, lessons learned, and key guideposts to consider**<br> 
**2:30-2:50: Self Reflection**<br>

- **Exercise:** Let’s brainstorm some questions you could ask during your fellow supervisor meeting…<br>

    - To understand your positionality → Where do you fit into the community?<br>
    - To identify those in the community with connection to other relevant community members → How can you set yourself up to be able to work with them to make new connections?<br>

**BREAK**<br>
**3:00-5:00: DEI at OpenTEAM: Inequity in Agriculture**

### Community Engagement Resource Overview

#### Before we begin:

The following curriculum provides a high-level overview of three community engagement tools: Community Mapping, Needs Assessment, and Seasonal Calendars. This document is designed as a jumping off point for further engagement at a later point. Following this overview, we will be working through a community mapping exercise developed for Wolfe’s Neck Center, focusing on both internal and external communities, to offer an example of what it could look like to apply these ideas on the ground.
<br><br>
Throughout this day, think about what could be useful to discuss with your Fellow Supervisor during your one-on-one meeting on Friday! There will be dedicated time at the end of today’s session for self-reflection and planning.<br>

#### Things to consider as we work through this exercise:

- This is community development-specific content, a majority of which was pulled from Peace Corps documentation. Although some examples are Peace Corps-specific, they are helpful to set an ideological foundation for later application.<br>
- Keep in mind the unique structure of your OpenTEAM Fellow communities:<br>

    - Internal: Hub organizational level (Fellow supervisor, coworkers, etc.)<br>
    - External: Greater hub network level (Farmers/ranchers/producers, partner organizations, etc.)<br>
    - Greater OpenTEAM Network (Tech developers, other hubs, OT Fellows group)<br>

#### Key resources

The following information was adapted from the following resources, use these resources to dig deeper and explore on your own later.<br><br>

- Peace Corps: The Participatory Analysis for Community Action Field Guide: [PACA Guide](https://files.peacecorps.gov/documents/paca-field-guide-for-volunteers.pdf)<br>
- The Community Mapping Toolkit: <i>[Community Mapping Toolkit](https://ucanr.edu/sites/CA4-HA/files/206668.pdf)</i><br>
- Best Practices Brief: [Several Forms of Community Mapping](https://outreach.msu.edu/capablecommunities/documents/CommunityMapping2.pdf)<br>
- [Comprehensive Needs Assessment](https://www2.ed.gov/admins/lead/account/compneedsassessment.pdf)<br>
- [Commons Library: Community Mapping](https://commonslibrary.org/community-mapping/?gclid=CjwKCAjwquWVBhBrEiwAt1Kmwuup-nblNhIzdatFXtEqLyPrAAC_-Vztk0rSI9ifVpZxYfQ5z-DIDhoCT2QQAvD_BwE)<br>
- [OpenTEAM Equity Collabathon Resource Guide](https://openteam-equity-resource.webflow.io/)<br><br>

<u>Resources from Group Discussion:</u><br>

- Ethics of Care: Patricia Hill Collins: [Black Feminist Thought](https://www.routledge.com/Black-Feminist-Thought-Knowledge-Consciousness-and-the-Politics-of-Empowerment/Collins/p/book/9780415964722)<br> 
- [Deep Canvassing](https://deepcanvass.org/)<br>
- AORTA Facilitation Guide and Community Agreements: [ANTI-OPPRESSIVE FACILITATION FOR DEMOCRATIC PROCESS](https://arts-campout-2015.sites.olt.ubc.ca/files/2019/02/AORTA_Facilitation-Resource-Sheet-JUNE2017.pdf)<br>

#### Overarching Overview: Why are we defining community?

Defining community, what and who are included in this definition, what and who lies outside of what you consider community in this context.<br>

- <i>“Community Mapping is a process which enables people to gather accurate information about how a community operates in order to mobilize its members and put them in a better position to represent themselves.”</i> - <a href= "https://commonslibrary.org/community-mapping/?gclid=CjwKCAjwquWVBhBrEiwAt1Kmwuup-nblNhIzdatFXtEqLyPrAAC_-Vztk0rSI9ifVpZxYfQ5z-DIDhoCT2QQAvD_BwE">Commons Library</a><br>

#### About community engagement tools and when/why to use them:  

There are many participatory, community mapping, agricultural extension-based, and community outreach and facilitation tools out there and available - you may find or already have other tools for working within a community that resonate with you.<br><br>

When deciding which tool(s) to use, consider your objective:<i> What insights, results, actions, direction, or clarification do you hope to gain? </i><br>

#### Positionality - Where do you fit into the community?

- Power / DEI resources and reflection questions in the resource guide, opportunity for this to be made more complete! We will discuss further during the afternoon DEI session.<br>
- Problem-based vs. Participatory strength-based approaches (PACA Guide, p. 6).<br><br>

<img src = https://gitlab.com/OpenTEAM1/fellows-tutorials/-/raw/master/Screen_Shot_2022-07-19_at_3.56.15_PM.png><br>

### Community Mapping & Needs Assessment

#### Definition of Community Mapping:

<i>Community mapping is a participatory process to understand the people and groups/institutions within a community, their talents, connections, and values, and how they relate and interact with each other. Developing a drawing of these interconnections can help visualize and understand existing social networks within a community.</i><br><br>

<i>This process can encourage community members to become powerful advocates for the transformation of the spaces in which they live/work, becoming more civically minded in the process. It can also foster a sense of environmental and community responsibility in local residents (p 4, <a href = https://ucanr.edu/sites/CA4-HA/files/206668.pdf> Community Mapping Toolkit </a>, p 34, <a href = https://files.peacecorps.gov/documents/paca-field-guide-for-volunteers.pdf> PACA Guide</a>).</i>

#### Objectives:

- Identify stakeholders, collaborators, and partners and their roles, skills, and capacity<br>
- Identify physical (and digital) community assets and their value to the various players identified above<br>
- Understand individual positionality within the community (role, power, biases…)<br>
- Identify role<br> 

#### Key Themes:

- Starts with what is present in the community<br>
- Concentrates on the agenda-building and problem-solving capacity of the residents <br>
- Stresses local determination, investment, creativity, and control<br>

#### Approaches:

- <u>Community ASSET Mapping</u> - Process of inventorying the resources or assets available to a specified organization or community.<br>

    - Every community has assets; facilities such as libraries and community centers, valued businesses, parks and forests are obvious. But most importantly the people and their capacities; organized community groups or individuals who have skills and talents.<br> 
    - All of these things can be mapped to create a picture of the community which shows its capacity and its potential. (p 4, [Community Mapping Toolkit](https://ucanr.edu/sites/CA4-HA/files/206668.pdf), p 1 & 2, [Several Forms of Community Mapping](https://outreach.msu.edu/capablecommunities/documents/CommunityMapping2.pdf))<br>

- <u>Community RELATIONSHIP mapping</u> - Visual presentation of relationships. The mapping of inter-organizational linkages is a form of ecomapping designed to show the relationships that one organization has with other organizations within the community.<br> 

    - Relationships with other organizations may relate to funding, referrals, access to resources, joint service planning, collaborative projects with contributed staff or funds, etc.<br> 
    - Relationship mapping can clarify the place of an organization in the community spectrum, to identify gaps in linkages, to indicate the multiple relationships between organizations, etc. (p 4, [Several Forms of Community Mapping](https://outreach.msu.edu/capablecommunities/documents/CommunityMapping2.pdf))<br>

### Needs Assessment:

#### Definition:

A “Needs Assessment” can take many forms but generally can be the natural next step following community mapping. Once a community map of assets, people, and relationships is developed, the next step is to work with key collaborators or stakeholders to understand their goals and what needs to happen to reach those goals. Meetings and conversations can be used to identify the discrepancy between the present and desired states.<br>

#### Key Terms:

- A <u>“Need”</u> is a discrepancy or gap between “what is” and “what should be.” <br>
- A <u>“Needs Assessment”</u> is a systematic set of procedures that are used to determine needs, examine their nature and causes, and set priorities for future action. <br>
- <u>“Target Group”</u> Needs Assessments are focused on particular target groups in a system. Common target groups in education settings include students, parents, teachers, administrators, and the community at-large. <br><br>

**Link to detailed worksheet on the “Three-Phase Model Approach to Needs Assessment”:** 
<i>See pages 8-19 of <a href = https://www2.ed.gov/admins/lead/account/compneedsassessment.pdf> Comprehensive Needs Assessment</a> for more information and worksheets</i>

### Seasonal Calendar:

#### Definition:

Understanding the seasonal changes in agricultural tasks, organizational labor requirements, and how different seasonal requirements and constraints affect farmers and community members can help develop realistic goals, work plans, and develop a deeper understanding of stakeholders needs and capacities (p 40, [PACA Field Guide](https://files.peacecorps.gov/documents/paca-field-guide-for-volunteers.pdf)).
<br><br>
<figure>
 <img src = https://gitlab.com/OpenTEAM1/fellows-tutorials/-/raw/master/docs/img/Screen_Shot_2022-07-19_at_4.52.50_PM.png>
 <figcaption><i>Example seasonal calendar from Peace Corps. </i></figcaption>
 </figure>


#### Objectives:

- Describe the context of the group (i.e. hub organization, regional ag, a specific farm/ranch) you will be serving in and how it varies throughout the year<br>
- Understand how constraints and the seasonal calendar impact stakeholders and different groups within the community<br>


#### Generalized Approach:

- Identify different spheres of activity and tasks (i.e. agricultural production types, organizational programs, research, specific projects, etc.)<br>
- For each sphere of activity identify related tasks (i.e. dairy production includes tasks related to milking, haying, etc.)<br>
- Place the tasks associated with each sphere of activity on a calendar, identify key stakeholders<br>
- Observe variation in labor and capacity seasonally, begin to identify areas of increased flexibility, gaps, potential challenges to consider for your own work<br>

### GROUP DISCUSSION

**Key Questions to Consider:** <br>

- What are your initial impressions of these tools?<br>
- What are your thoughts on their intersection with the work OpenTEAM does? And your role as a fellow within a community?<br>
- Do you anticipate any challenges in using/applying these tools?<br>
- What other approaches/tools have you used when working with communities? How are they similar or different to these?<br>
- What principles or values do you see as most essential for working within a community?<br><br>

### Community Themes Activity

Community Themes*:<br><br>

- Learn from your community & Each Other<br>
- Storytelling:<br>
    
    - Highlight knowns/unknowns<br>
    - Transparency/reflexivity<br>

- Know your audience<br>
- Build relationships to:<br>

    - Build trust<br>
    - Find synergies<br>
    - Alongside existing relationships within community and to establish context/history<br>

- Active listening<br>
- Lead with curiosity/Ask Q’s/Willingness to learn<br>

    - Empathy<br>
    - Compassion<br>

- People come first → Farming is a community of people (not a business, its people)! (Unlike other industries) Empathy matters!<br>

    - Think about the environment you’re entering AND creating (Love music, food…)<br>

- Conscience of schedule (seasonal calendar and flows of work)<br>
- Be away of other flows of informational highways/forums/meeting places<br>

    - Formal and informal (workshops, forums/facebook pages, bars, BBQs, Church)<br>
    - Find a connection to..<br>

- Burnout is not good. Lean on your community.<br>
- Tap into existing skills and relationships, don’t reinvent the wheel!<br>
- Tools not Rules!<br>
- Positionality, bias, reflexivity!<br> 
- Respect → Treat farms as the farmers home<br>
- Center your conversations on key information: What does this person need to know?<br><br>

*This list was generated through group discussion during a Fellows Orientation session. As you move through your fellowship please add other themes, thoughts, and words of wisdom in this [Living Document](https://docs.google.com/document/d/1olXa-GcxH5DKRVqQxVHcgRzbhsGRPDwf4g13MfgaRug/edit?usp=sharing). <br><br>