# SurveyStack

SurveyStack is a flexible data collection, management, and analysis platform, designed by Our Sci LLC with community-driven research in mind. Learn more about SurveyStack [here.](https://www.youtube.com/watch?v=MnU6xsdumlc)

<img src = https://gitlab.com/OpenTEAM1/fellows-tutorials/-/raw/master/docs/img/Screen_Shot_2022-07-17_at_7.38.02_PM.png>

## Additional Resources

  - [SurveyStack Tutorials](https://our-sci.gitlab.io/software/surveystack_tutorials/)
    - [Group Basics](https://our-sci.gitlab.io/software/surveystack_tutorials/groups/)
    - [Best Practices](https://our-sci.gitlab.io/software/surveystack_tutorials/best_practices/)
    - [Tour the Survey Builder](https://our-sci.gitlab.io/software/surveystack_tutorials/survey_builder_tour/)
    - [Create a New Survey](https://our-sci.gitlab.io/software/surveystack_tutorials/create_new_survey/) 
    - [Question Types](https://our-sci.gitlab.io/software/surveystack_tutorials/question_types/)
    - [Relevance Expressions](https://our-sci.gitlab.io/software/surveystack_tutorials/relevance_expressions/)
  - [Check Out The Question Set Library](https://www.our-sci.net/check-out-the-question-set-library/)
- [SurveyStack Common Profile Glossary](https://docs.google.com/document/d/1RIR_urxPX4bPClGz04YvTME7wv-5DZjovEI8jMLFit4/edit)
- [Guide for Fellows to Onboard Producers to Hylo Through Surveystack](https://docs.google.com/document/d/1oUsySFDRzHzDjE8hqPvXWww4-AhO3leba8EfZ43yIlc/edit?usp=sharing)
- [SurveyStack Fellows Tutorial](https://docs.google.com/document/d/1zH0b72HwDwHRVQED9OCYUvBpECy6NSqWpkDXaZnYahE/edit?usp=sharing) 
- [Fellows Orientation Admin Session](https://drive.google.com/file/d/1YpQ5qxN1pZQ3CnbRy0w6uIxMbi4g4bJn/view?usp=sharing)
- [Fellows Orientation Common Enrollment Session](https://drive.google.com/file/d/1Yj9eHRTtS_ixOezupu_iwzh_cK_BbCJS/view?usp=sharing)

## Developer Walk-Through (with Dan TerAvest)

 <p style="color:#FF0000";><strong>From OpenTEAM Fellows Orientation -- July, 2022</strong></p><br>

### SurveyStack Admin Session

#### SurveyStack Overview 

- Basic overview of Survey Stack (10 minutes)<br>
- Understanding Survey Stacks role in the Fellowship e.g. common onboarding (5 minutes)<br>

#### Create Account & Enter Group

- Fellows will be invited to the OpenTEAM Fellows Training Group<br> 
  - Discuss levels of permissions, [group basics](https://our-sci.gitlab.io/software/surveystack_tutorials/groups/) & magic links<br>

#### Learn Integrations

- Demo how to create a test farmOS instance & add all fellows to that instance<br>
- Fellows will add a Hylo integration<br>

    - Follow the [Guide for Fellows to Onboard Producers to Hylo Through Surveystack](https://docs.google.com/document/d/1oUsySFDRzHzDjE8hqPvXWww4-AhO3leba8EfZ43yIlc/edit?usp=sharing) from the Hylo team

#### Build Common Onboarding Surveys with the Common Onboarding Question Set for Hub Assignments

**These will be drafts for each hub, possibly deleted and restarted later.**

- Review the [SurveyStack Common Profile Glossary](https://docs.google.com/document/d/1RIR_urxPX4bPClGz04YvTME7wv-5DZjovEI8jMLFit4/edit)
- Follow the [SurveyStack Fellows Tutorial](https://docs.google.com/document/d/1zH0b72HwDwHRVQED9OCYUvBpECy6NSqWpkDXaZnYahE/edit?usp=sharing) to build your own survey.<br>

    - Navigate to the Builder and draft a Common Onboarding survey<br>
    - Find the Common Onboarding question set in the question set library<br>
    - Pull the common profile question set from the question set library into survey<br>
    - Review how to edit common profile question set<br>
    - Understand data name vs. data label<br>
    - Learn how to modify the Onboarding survey and question set (add new question, hiding unnecessary question)<br> 

#### Invite members to take your survey

### SurveyStack Common Onboarding Session

#### Fellows will complete our sample [Fellows Training Common Onboarding Survey](https://app.surveystack.io/surveys/62cc2f1264216000015a208c) 

- Note, this will push to the test FarmOS that Dan creates & links in the first session<br>
- Note, this will also create Hylo Farm Profile pages that we will eventually want to delete, along with the Fellows Training Group that will be integrated in the SurveyStack Admin.<br> 

    - Fellows can keep their individual accounts created at this time<br>

#### SurveyStack Results

- How to view & download results (download, spreadsheet, etc.)<br> 
- Overview of how these results are being used<br>
- Sorting feature<br>
- Resubmitting to fix errors<br>
- Where to go for help<br>

#### Q&A (on all things SurveyStack)

### Future Training Sessions

- How to create your own question set in detail - more options, conditional questions, calculations, etc.<br>  
- How to push a question set to the question set library<br>
- Create a question set to add to the common profile question set to push to farmOS & Hylo<br><br>

