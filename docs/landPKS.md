# LandPKS (Land Potential Knowledge System)
<img src = https://gitlab.com/OpenTEAM1/fellows-tutorials/-/raw/master/docs/img/LandPKS_Logo.png>

[LandPKS](https://landpotential.org/) is a USDA-backed tool used for learning about the land and growing potential of specific sites around the world.<br><br>

<p style="border:3px; border-style:solid; border-color:#000000; padding: 1em;">Access Climate, Wildlife Habitat Management, Soil Conservation information and more!<br>Automatically calculate Available Water Holding Capacity and Infiltration based on texture and organic matter.</p>

<br>
<img src = https://gitlab.com/OpenTEAM1/fellows-tutorials/-/raw/master/docs/img/LandPKS_Features.png>

## Additional Resources

<ul>
<li><a href="https://www.youtube.com/watch?v=ODymv3nWbH0">Brief primer on the tool from USDA</a></li>
<li><a href= "https://landpotential.org/knowledge-hub/">LandPKS Knowledge Hub</a></li>
<li><a href="https://www.youtube.com/watch?v=sEVSm7I0zms">Learn more from the GOAT webinar series</a></li>
</ul>
<br>
**A Note on Feedback:**<br>
LandPKS is in the process of designing the next generation of their platform. For this reason, feedback provided by fellows in the coming months will have a particularly large impact by helping to potentially improve the current app, as well as informat the next generation of LandPKS tools. Any and all feedback is welcome and appreciated! 

## Onboarding Process

 <p style="color:#FF0000";><strong>Last Updated July, 2022</strong></p><br>

For this activity, you will be inputting information collected from the Wolfe’s Neck fields in order to get a better idea of how LandPKS works, and the cases in which it would be most useful to producers.<br><br>

Throughout your use of the LandPKS app, you may encounter new terminology or techniques that require additional learning. **There are question marks throughout the app that will provide you with context and text/photo descriptions of the steps needed.**<br> 

#### Required Equipment
- Smartphone with LandPKS downloaded
- Shovel
- Trowel or soil knife (optional)
- Ruler or tape measure
- 3M Canary Yellow post it notes (for soil color)
- Tarp marked with sampling depths (optional)
- Gloves (optional)
- Full bottle of water (ideally with a squirt top or sprayer)

#### Step 1: Create Your Account

**1.1.** Download the Land PKS app onto your phones <br>
**1.2.** Create Your LandPKS Account <br>
**1.3.** Note with account (Apple or Google) you use to log in so that you can use this at your Hubs as well<br>
      

#### Step 2: Explore App Settings (Under More>Settings) including units, language and soil color reference (Yellow Post-it)

**2.1.** Make a note of where you can send a bug report to developers <br>

- *Also be aware of [OpenTEAM's feedback and support processes](/fellows-tutorials/feedback%2Bsupport/)*

--- 
> #### Worksheet # 1
>
> <strong>Explore the app functionalities, and record:</strong> 
> </br>
> <ul>
> <li>Annual precipitation for 2019:</li>
> </br>
> </br>
> <li>Monthly precipitation for July:</li> 
> </br>
> </br>
> <li>Average temperature for July:</li>
> </br>
> </br>
> <li>How do these numbers compare to what you’re experiencing now?</li>  
> </ul>
> </br>
> </br>
> <strong>Explore the Soil ID for Wolfe’s Neck:</strong> 
> <ul>
> <li>Navigate to the Map tab. Create a plot for the field location that you are going to visit. You may have to wait while data downloads.</li>
> <li>Once the site loads, open the LandInfo tab. Note the soil type, top layer color, and texture for the highest probability soil based on the app's prediction (data from NRCS soil map).</li><br><br><br> 
> <li>Note the top layer texture based on SoilGrids (an international digital soil map).
> </br>
> </br>
> </br>
>    <li>Are the predictions of the NRCS soil map and digital soil map the same?</li><br><br><br>
>    <li>Do all members in your group get the same results? If not, check GPS coordinates, especially if oyu have recently traveled a long distance.</li><br><br>
> </li>  
> </ul>

--- 

#### Step 3: Soil Identification - Will the real soil identify itself (with the help of LandPKS)


**3.1.** Now it’s time to head out into the fields! Go to the location for which you created a site in the previous exercise and begin the identification process.<br>
**3.2.** Open the site that you already created for this location (or if you haven't created one, click on the "+" to create one).<br>

<ul>
<li><i>Notice the “Private” toggle. The default is set to share the data on the <a href="https://portal.landpotential.org/#/landpksmap">LandPKS database.</a></li> <li>For the purpose of this activity, we will keep the setting to “Private” off to share our data.</i></li>
</ul>

**3.3.** Click the Data Input tab.<br>
**3.4.** Following the directions in the question mark text throughout the module, enter soil texture and rock fragment volume for at least the top 50cm (20”).<br>
**3.5.** Sync.<br>
**3.6.**  Review the new information in LandInfo in the Report tab for “Your Data”. Did the top-ranked soil change?<br>    


#### Step 4: Explore the rest of the app

**4.1.** Notice you are on the “Report” tab on the top<br>
**4.2.** Look through each of the drop down menus<br>

- *LandInfo*
- *WOCAT Technologies*
- *Vegetation*
- *Habitat*
- *Land Management*
- *Soil Health*

**4.3.** Do the same for the Data Input tab.<br>
**4.4.** Notice the headings: “LandInfo”; “LandManagement”; “Soil Health (Beta)”; “Vegetation”; “Photos and Notes”<br>
**4.5.** Focus on LandInfo and SoilHealth input tabs. If you will be working with ranchers, explore the Vegetation tab and supporting training videos.

<details>
<summary><strong><i>Click here for guidance on data entry.</i></strong></summary>
<br>
<strong>LandUse</strong><br><br>
<ul>
<li>Click on LandUse and click the correct option for LandCover and Grazing that best matches your site</li>
</ul><br>
<strong>LandSlope</strong><br><br>
<ul>
<li>Follow the instructions to use the slope meter to determine the slope of your site</li>
</ul><br>
<strong>Soil Texture</strong><br><br>
<ul>
<li>For this activity you will assess texture for 0 -1 cm and 1 - 10 cm</li>
<li>Click the depth and click “Guide me” to conduct texture by feel for each depth</li>
</ul><br>
<strong>Soil Limitation</strong><br><br>
<ul>
<li>Fill out each of the soil limitations</li>
<li>Remember to click the question mark to get an explanation of the indicator</li>
</ul><br>
<strong>Soil Color</strong><br><br>
<ul>
<li>For this activity you will assess color for 0 - 1 cm and  1 - 10 cm</li>
<li>Click on the desired depth and click “start”
<ul>
<li>Note: you will need your canary yellow post it note for this!</li>
</ul></ul>
<strong>LandManagement</strong><br><br>
<ul>
<li><strong>Please note that this is the one function that is not currently planned for the NextGen LandPKS. But, please let us know if you think it should be after reviewing the other OpenTEAM tools that support recordkeeping.</strong></li>
<li>Open “Field Description” and enter data from your worksheet</li>
<li>Explore the field calendar, click on today’s date and explore the options</li>
<li>Add in the data from your worksheet for activities for this week</li>
</ul><br>
<strong>Soil Health (Beta)</strong><br><br>
<ul>
<li>Open “Field Calendar”</li>
<li>Click on Today’s date, open “Field Observations”</li>
<li>Explore your options, pick two observations for today’s date to complete</li>
</ul><br>
<strong>Vegetation</strong><br><br>
<ul>
<li>Open ”Field Calendar”</li>
<li>Click on Today’s date and explore</li>
<li>Take note of the data you can collect here</li>
</ul><br>      
<strong>Photos & Notes</strong><br><br>
<ul>
<li>Take a photo of your soil pit (Pit Photo)</li>
<li>Take a photo of your team (Landscape Photo)</li>
</ul><br>
</details>
--- 
> #### Worksheet # 2
>
> In order to gain a more robust understanding of the use cases of these tools, we are going to compile a Use Case document as a reference. Discuss these questions about the applicability of this tool, using online resources and your experience using LandPKS:
> </br>
> </br>
> <ul>
> <li>How can this tool help a farmer or rancher?</li>
> </br>
> </br>
> </br>
> </br>
> <li>For whom is this tool designed?</li>
> </br>
> </br>
> </br>
> </br>
> <li>What kind of farmer is this tool good for?</li>
> </br>
> </br>
> </br>
> </br>
> <li>What kind of farm is this tool good for?</li>
> </br>
> </br>
> </br>
> </br>
> <li>When would it be most useful?</li>
> </br>
> </br>
> </br>
> </br>
> <li>What are the tool’s privacy policies?</li>
> </br>
> </br>
> </br>
> </br>
> <li>How does this tool interact with other OpenTEAM tools (e.g. SurveyStack, Hylo, the Ag Data Wallet, Digital Coffeeshop, etc.)</li>
> </br>
> </br>
> </br>
> </ul>

--- 

