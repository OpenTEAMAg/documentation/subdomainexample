# COMET-Farm

<img src = https://gitlab.com/OpenTEAM1/fellows-tutorials/-/raw/master/docs/img/Screen_Shot_2022-07-15_at_12.03.06_PM.png>

<br>
[COMET-Farm](https://comet-farm.com) was designed by Colorado State University and USDA for conservation scenario analysis within the farm-gate. COMET-Farm is a farm- and field-level greenhouse gas and carbon accounting tool. In contrast, Cool Farm Tool is a life cycle assessment tool that can be used to track GHG emissions for a specific farm product (i.e.kg of milk) OR a whole-farm biodiversity assessment. 

## Additional Resources

<ul>
<li><a href="https://drive.google.com/file/d/12aLbf9gXsnW41k2jJ6sAX-e_pkNSSFPN/view?usp=sharing">Overview of tool</a></li>
<li><a href= "https://www.youtube.com/watch?v=oraAbq8vpKU">GOAT webinar series</a></li>
<li><a href="http://comet-farm.com/HelpPage">COMET PDF tutorials and training videos</a></li>
</ul>

## Onboarding Process

 <p style="color:#FF0000";><strong>Last Updated July, 2022</strong></p><br>

The goal of this activity is to expose you to as much of the COMET-Farm tool as possible. You will be asked to enter additional data to the existing WNC project. As you work through, keep in mind that you are only entering information for one parcel of land (which represents one field). If you were to create a project from scratch, you would need to collect information to enter each field or parcel of land for an entire farm (or whatever part of the farm you are analyzing). This way, you will be able to navigate through the entire tool to learn what information is required, how the tool is structured, and how to enter data, to get an idea of what it would look like to do this for an entire farm.

#### Required Equipment
- Computer

#### Step 1: Creating an Account

For this exercise you will be using the WNC’s COMET account. For future reference, here is a [handout](http://comet-farm.com/images/tutorial/pdf_tutorials/1%20Creating%20a%20COMET-Farm%20Account.pdf) explaining how to register a new account.<br>

- *Note: users can use COMET without registering an account, but the information entered during a session will not be saved.*<br>

    - Follow the link above to the COMET-Farm website. Click ‘Log-in’ in the top right corner, use the information below to sign-in.<br> 
    - User name: OpenTEAM_WNC<br>
    - Password: Openteam123<br>

#### Step 2: Creating a Project
You will not be required to create a new project for this activity. You will be building off the project already created for the WNC farm. For future reference, if you were to ‘Create New Project’ instead, you would be asked to 1) define which activities you want to analyze (cropland, animal agriculture, agroforestry, or forestry) and 2) enter an address/zip code/coordinates to orient the map to the farm/ranch location.<br>

- Under ‘Select a Project’, select ‘Wolfe’s Neck Center.’ Next, to the right of this list, make sure both cropland and animal agriculture tabs are selected. Click ‘Define Activities’ to enter the data input portal. 

#### Step 3: Data Entry 

After opening the input portal, you will be prompted to enter data for different sections. The first section is ‘Field Management.’ You will land on a page called ‘Parcel Locations’ which shows a map of WNC. Most parcels have already been drawn to outline hay fields and grazing pastures at WNC. Zoom in and out to get a feel for the scope of the farm and the different parcels.<br><br>

**Adding Parcels to Map**<br><br>

COMET requires parcel boundaries (for fields, pastures, etc. included in the assessment) in order to derive soil and climate information.<br><br>

Parcels can be added by a single point in the approximate center of the parcel, by drawing a circle, or by creating a polygon. A point can be used to define relatively small (e.g. less than 10 acre) fields or features, or for center pivot systems. A polygon is best used for non-circular fields larger than 10 acres (like The Shire!).<br>

- *Read through this 2-page [guide](http://comet-farm.com/images/tutorial/pdf_tutorials/3%20Cropland%20Parcel.pdf) to learn more about parcels and helpful map features. (This guide can also be found on the COMET-Farm.com ‘Help’ page linked under Additional Resources at the top of this page).*<br><br>
 
**3.1.** Add a parcel “by polygon” behind the Farmhouse/Barn (the main office building where orientation is held) to represent a field referred to as ‘The Shire.’<br> 

- *Use [this picture](https://drive.google.com/file/d/1TgBVuCc7mHMz-GgtsK2B7pj4M2-9yGZs/view?usp=sharing) as reference. Sheep graze here during the Summer, but it is not currently captured within COMET.*<br>

**3.2.** When you are done click “I am done defining parcels.”<br>
**3.3.** This action will bring you to the ‘Historical Management’ section. There is a smaller map of the parcels on this page. See the color legend underneath.  Use the drop down above the map to select “The Shire” parcel. Compared to the other parcels on the map, The Shire should be blue, indicating the data entry is incomplete.<br>

- Open this [google doc](https://docs.google.com/document/d/1A-JQjtXwQvZZkxBexH9jrFbT2ThLbonapacGwXuxXac/edit?usp=sharing) for WNC information you need for data entry.<br> 
    - *Enter the historical management data for The Shire. Refer to [this guide](http://comet-farm.com/images/tutorial/pdf_tutorials/4%20Cropland%20Historic%20Management.pdf) as you enter each piece of information.*<br>

**3.4.** Once complete, hit the orange ‘Next’ on the bottom right to navigate to the next page ‘Baseline Management.’ Again, make sure the parcel selected is The Shire. <u>Use the same google doc linked above to access WNC information for Baseline Management data entry.</u> Refer to [this guide](http://comet-farm.com/images/tutorial/pdf_tutorials/5%20Cropland%20Current%20%20Management.pdf) as you fill in the baseline management information.<br>

- When complete, select ‘Skip Ahead’ at bottom.<br>
- When prompted to add another crop select ‘No thanks, continue.’ (At this point you will have entered all the crops you need).<br>
- You will be prompted to ‘copy crop?’ You will want to do this to save time…or else you’d have to enter this same information for every year from 2000 to 2022.<br>
- When prompted to move onto ‘Future Management’, click ‘Keep Editing.’ Then manually navigate to the ‘Animal Agriculture’ tab at the top to move-on. *You will not be entering a future management scenario during this activity, but keep this feature in-mind!*<br>

**3.5.** Now you will input data in the ‘Animal Agriculture’ section.<br>

- When prompted by the pop-up window, select Property Location<br>

    - Enter Zipcode: 04032. ‘Save and Continue’<br>

- Select Animal Types<br>

    - All animals at WNC are already captured here. For this activity you will need to review the flock sheep inputs, as we need to add the three lambs born this Spring to the assessment.<br>
    - Don’t adjust anything on this page. Just click ‘Save and Continue’<br>

- On the next page, under ‘Enter Animal Details’ click on ‘Flock Sheep’<br>

    - Add 3 more sheep to the count for every month. Note: Although the sheep were acquired in the Spring, we are using this assessment as an annual snapshot. Therefore we are entering the total number of sheep for all months.<br>
    - All other inputs will stay the same, but navigate through the fields using ‘Save and Continue’ to review what these are.<br>


#### Step 4: Review Report

You are now done with the data input process of the assessment. If you were completing an assessment for an entire farm, you would want to add all land parcels and animals before moving onto the whole-farm report. 

**4.1.** Click on the ‘Report’ tab. *Note: the report page can be slow to load.*<br><br>
**4.2.** Click through the different tabs within the report.<br>
**4.3.** Then, pull up the Results from your CFT assessment so you can compare.<br>

- Look at the total emission #s each tool provides. What do you notice about how the results differ between tools? *(After you think about this on your own, read the italicized info below).*<br>
- For what scenarios/types of assessments do you see each tool being most helpful for?<br>

    - Specifically, what type of analysis could the Future Management feature in COMET be used for?<br>

- Would you want to see the results in a different format? If so, how and why would that be useful?<br>
<br>
*Although both tools take into consideration similar inputs (land management, feed inputs), the scope of the results is the biggest difference between the tools. COMET takes into consideration land management changes over time on a whole-farm basis, providing results for emissions by GHG type for current management . On the other hand, CFT takes into consideration emissions of every activity that goes into creating a specific product (feed production, fertilizer applications, release of GHG by animals, fuel used to transport feed and other inputs, energy used on-farm, etc.) and produces a yearly snapshot of every source of emissions.*

    
--- 
> #### Worksheet # 1
>
> <strong>Discuss these questions about the applicability of this tool, using online resources and your experience using COMET:</strong> 
> </br>
> </br>
> <ul>
> <li>How can this tool help a farmer or rancher? </li>
> </br>
> </br>
> <li>For whom is this tool designed? 
> </br>
> </br>
> <ul>
> <li>What kind of farmer is this tool good for? </li>
> </br>
> </br>
> <li>What kind of farm is this tool good for?</li>  
> </br>
> </ul>
> <li>When would it be most useful? 
> </br>
> </br>
> <ul>
> <li>When would you use the app on a computer vs. on a mobile device? </li>
> <br>
> <br>
> </ul>
> <li>What are the tool’s privacy policies?</li> 
> </br>
> </br>
> </ul>

--- 






