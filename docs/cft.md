# Cool Farm Tool 

<img src = https://gitlab.com/OpenTEAM1/fellows-tutorials/-/raw/master/docs/img/Screen_Shot_2022-07-15_at_11.38.21_AM.png>

[Cool Farm Tool (CFT)](https://coolfarmtool.org/) can be used to create three main assessments. <br>

- First, it can be used as a farm product greenhouse gas accounting tool, taking into account nutrients added, energy used, and land-use activities to create a product (milk, apples, etc.). It is applicable to almost all crops and animal livestock globally. Updates are currently underway to support perennial and multi-year crops like coffee, tea, orchard, and berries. 
- Second, it can be used to quantify how farm management practices support biodiversity. 
- Third, you can analyze crop irrigation needs to optimize water inputs and create blue (ground/surface water) and green (rainfall) water footprints. CFT is free to farmers. Organizations that use the tool to support sustainable agriculture can become members of the Cool Farm Alliance. Member fees help support the development and maintenance of the tool.

## Additional Resources

<ul>
<li><a href="https://drive.google.com/file/d/1NryJ47OA4HezFHEc1dfmbTMEne66V8e1/view?usp=sharing">Overview of tool</a></li>
<li><a href= "https://www.youtube.com/watch?v=b0675z3XAng">GOAT webinar series (Original Video)</a></li>
<li><a href= "https://www.youtube.com/watch?v=SG9CXYQLHH0">GOAT webinar series (Updated Video)</a></li>
<li><a href="https://coolfarmtool.org/wp-content/uploads/2016/09/Data-Input-Guide.pdf">Review the scope of data inputs</a></li>
<li><a href="https://coolfarmtool.org/coolfarmtool/frequently-asked-questions/">Have questions? Find answers through the Cool Farm Alliance FAQ page</a></li>
</ul>
<br>

## Onboarding Process

 <p style="color:#FF0000";><strong>Last Updated July, 2022</strong></p><br>

Record keeping differs by farm - type of operation, different managers, capacity to keep records, requirements for grants, loans, etc. This exercise will help you familiarize yourself with the information required to complete a GHG analysis on CFT. Once on-site and working with farmers to create CFT accounts and assessments, it will be important to have conversations to understand who tracks this information, how they manage it (excel sheets, shared files, hard copies), and how you can most easily access it. If data is unavailable, yet there is a desire to complete a GHG analysis, this could initiate a conversation about implementing or improving record keeping.

#### Required Equipment
- Computer

#### Step 1: Creating Assessments

**1.1.** Log into WNC’s Cool Farm Tool account → Click on the ‘My Assessments’ tab at the top of the page, open ‘Holstein_2021_PRACTICE.’<br> 

- *Note: To create a new assessment, Click ‘New Assessment’ and select assessment type.*<br>

**1.2.** Click through tabs to familiarize yourself with the data inputs (hover over ‘info’ buttons to learn more about each input field, refer to Data Input Guide linked above). <br>

#### Step 2: Data Entry 

Use information from the Excel table (linked below) and PastureMap (log-in below) to populate indicated fields in Milk, Herd, Grazing, and Feed tabs.<br> 

- *Note: When entering actual data, writing data input dates and notes to the ‘Notes’ box is very helpful. You will see notes in the practice account, please do not add/adjust these, thanks.*<br><br>

**2.1.** Milk: [Excel table](https://docs.google.com/spreadsheets/d/1aqkKHIk3M9VI4IivYE-nu920HvUmWo4nGe4ezHQ7cUU/edit?usp=sharing)<br> 

- Enter milk production using Total from ‘Production’ tab.<br>
- *Stonyfield picks up milk from the WNC dairy parlor every few days. The driver records how much milk they collect, date, etc. Yearly total calculated.*<br>

**2.2.** Herd: [PastureMap](https://app.pasturemap.com/)<br> 

- Sign into the WNC’s PastureMap account via the log-in below.<br> 
- Navigate to the ‘Herd’ tab.<br> 
- Use the number of cows in each category and average weights listed in PastureMap to populate the information missing in CFT.<br>

**2.3.** Grazing: [PastureMap](https://app.pasturemap.com/)<br> 

- Navigate to the home page of PastureMap. Enter total acres into CFT.<br><br>

**PastureMap Log-in**<br>
    - Username: agulachenski@wolfesneck.org<br>
	- Password: @GW01f2022!

<br><br> 
    
--- 
> #### Worksheet # 1
>
> <strong>Review charts and tables under ‘Results’ tab. Discuss the following questions:</strong> 
> </br>
> </br>
> <ul>
> <li>What sources are the largest contributors to total GHG emissions? </li>
> </br>
> </br>
> <li>Based on previous knowledge and online research, what are some management decisions (and corresponding data inputs) that could be made to reduce these sources? </li> 
> </br>
> </br>
> <li>How do you see this tool being useful for a farmer? </li>
> </br>
> </br>
> <li>What challenges do you foresee in collecting and entering this data? What are some ideas to circumvent these challenges? </li>  
> </br>
> </br>
> <li>What about when interpreting and communicating the data?</li> 
> </br>
> </br>
> </ul>








