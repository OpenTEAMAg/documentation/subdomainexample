# Setting the Stage: Tech Ecosystem

### The Goal

OpenTEAM serves as a convenor, technical facilitator, and community steward for our open source, agricultural tech ecosystem. Through a collaborative, human-centered design process, we're working to create a completely open source, accessible, and interoperable suite of tools for agricultural management. 

### Tech Ecosystem Components
In order to achieve our broad technological goals, there are a number of technological components that must work together. Within our ecosystem, there are user-facing applications that will grant farmers the ability to enter, store, and share data through easy-to-use, open-source software. To make this all happen, there are also back-end tech components that create informational building blocks for those user-facing tools. At the core of all of this, OpenTEAM cares deeply about accountability, and serves a key facilitation and governance role in this tech ecosystem. This tech vision is meant to support positive economic, social, and environmental outcomes within the agricultural sector. 

<figure>
 <img src = https://gitlab.com/OpenTEAM1/fellows-tutorials/-/raw/master/docs/img/Tech_Working_Group_Spring__July_-_September__-_Broad_Tech_Vision.jpg>
 <figcaption><strong><i>Above is a visual of how these core tech ecosystem components interact.</i></strong></figcaption>
 </figure>

[Here is the link to this flow chart, which is easier to view in Miro.](https://miro.com/app/board/uXjVOvaqGtk=/?moveToWidget=3458764529182361512&cot=14)

### Forthcoming Interoperability

A key part of this system is interoperability, or the ability for multiple tools to connect and exchange information. Creating an open source ecosystem of digital tools is fundamental to allowing users to enter agricultural data once and use it for many different purposes.<br><br>

Developers within the OpenTEAM ecosystem are working hard to create more seamless interoperability between their tools with single sign-on account management. Eventually, the goal is to support one user identity across all OpenTEAM tools, via the Ag Data Wallet.<br><br>

**The Ag Data Wallet** <br><br>

Interoperatibility is the guiding principle behind the Ag Data Wallet, a platform where land stewards can easily share their data across technologies, tools, and organizations while retaining control and encouraging collaboration. The Ag Data Wallet is designed to maximize the value of data through efficient, secure, and cost effective data management at the user’s discretion. This means land stewards can determine which entities (if any) can access their data, how that data may be used or shared, and how that data may be aggregated. This further enables the measurement, verification, and reporting of outcomes on the farm, supporting land stewards’ management and decision-making.<br><br>

A wallet is a simple and convenient way to store your identification, business and membership cards, payment methods, accounts and contracts, and other important personal information. A wallet contains a lot of data about its owner, and each item in the wallet holds even more. So, just like a real-life wallet holds multiple cards used to transfer money to other entities, the Ag Data Wallet holds agricultural data that can be easily shared while specifying who maintains control.<br><br>

<figure>
<img src = https://gitlab.com/OpenTEAM1/fellows-tutorials/-/raw/master/docs/img/wallet.jpg>
<figcaption><strong><i>The ag data wallet (illustrated above) is a producer-facing product that's developing from the interoperability of tools in the OpenTEAM tech ecosystem.</i></strong></figcaption>
</figure>
<br>
**The Farmers' Digital Coffeeshop**<br><br>

Three of OpenTEAM's core developer teams - Hylo, FarmOS, and SurveyStack - have already created well-integrated data flows. Any day now, the Farmer's Digital Coffee Shop will be up and running!<br> 
<br>

Checkout [this dashboard](https://coffee-shop.onrender.com/) to play around with the current dashboard in development (this is not yet available for public use).<br><br>

**Learn more about what's to come in from Greg Austic's recent in-depth, [here](https://youtu.be/fR2SPn1lEfI)**<br><br>

<figure>
 <img src = https://gitlab.com/OpenTEAM1/fellows-tutorials/-/raw/master/docs/img/image__9_.png>
 <figcaption><strong><i>Above is the vision of the farmers' digital coffeeshop, with all collaborative and benchmarking capabilities. We're not here yet.</i></strong></figcaption>
 </figure>

### Your Role

 This is a crucial time for testing the developing tech in this ecosystem on the ground. As fellows, you'll provide invaluable user stories and feedback to help developers improve their services and work toward our common goal of developing the Ag Data Wallet, while also supporting the data and technology needs of your pilot hub. 
<br><br>